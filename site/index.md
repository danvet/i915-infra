# Intel GFX CI


## What Is It About?

We are continously testing [i915][i915-desc] (a Linux kernel device driver
for Intel Graphics) in an automated fashion, with a goal of having production
ready upstream.

## Hardware List

Our CI system is composed of multiple Intel machines spanning many
generations and display types. You can find our full hardware list here:

 * [list of machines](/hardware.html)
 * [raw data dump](/hardware/) (dmidecode, various debugfses, etc.)


## Pre-Merge Testing

**This is the crux of this CI system.** We test each patch that goes in our
driver ([i915][i915-desc]) or in the DRM drivers test suite we use ([IGT GPU
Tools][igt-desc]) before it lands in the repository and compare the results
with the [post-merge baseline](#git-trees-tested-post-merge) ([we filter out
known bugs](#results-filtering-and-bug-tracking)). This shifts the cost of
integration on the people making the change and helps us avoid time-consuming
bisection and reverts.

[i915-desc]:      https://01.org/linuxgraphics/gfx-docs/drm/gpu/i915.html
[igt-desc]:       https://gitlab.freedesktop.org/drm/igt-gpu-tools#igt-gpu-tools

Since we accept patches through mailing lists, this is where you can find the
results - they are sent out as a replies to the original mail. Here are the
mailing lists we currently support:

| Mailing List                           | List Info              | Archive                   | [Patchwork][patchwork-desc] |
| --                                     | --                     | --                        | --                          |
| intel-gfx@lists.freedesktop.org        | [info][intel-gfx-info] | [archive][intel-gfx-arch] | [patchwork][intel-gfx-pw]   |
| igt-dev@lists.freedesktop.org          | [info][igt-dev-info]   | [archive][igt-dev-arch]   | [patchwork][igt-dev-pw]     |
| intel-gfx-trybot@lists.freedesktop.org | [info][trybot-info]    | [archive][trybot-arch]    | [patchwork][trybot-pw]      |

[intel-gfx-info]: https://lists.freedesktop.org/mailman/listinfo/intel-gfx
[intel-gfx-arch]: https://lists.freedesktop.org/archives/intel-gfx/
[intel-gfx-pw]:   https://patchwork.freedesktop.org/project/intel-gfx/series/

[igt-dev-info]:   https://lists.freedesktop.org/mailman/listinfo/igt-dev
[igt-dev-arch]:   https://lists.freedesktop.org/archives/igt-dev/
[igt-dev-pw]:     https://patchwork.freedesktop.org/project/igt/series/

[trybot-info]:    https://lists.freedesktop.org/mailman/listinfo/intel-gfx-trybot
[trybot-arch]:    https://lists.freedesktop.org/archives/intel-gfx-trybot/
[trybot-pw]:      https://patchwork.freedesktop.org/project/intel-gfx-trybot/series/

[patchwork-desc]: https://gitlab.freedesktop.org/patchwork-fdo/patchwork-fdo#patchwork-fdo

### Queues

You can check our pre-merge queue for ([BAT](#basic-acceptance-tests-aka-bat)
only!) at <https://intel-gfx-ci.01.org/queue/>.

Intel-GFX-CI queue (kernel patches) has priority over IGT queue and will be
drained first. Trybot is best-effort and has the lowest priority.


## Git Trees Tested Post-Merge

We test many trees post-merge. Some of them are our baseline for pre-merge
testing (drm-tip and IGT GPU Tools), while the others helps us to make sure
that what we submit upstream works and catch any potential issues cased by
changes in other drivers/trees before we actually integrate with them.

The testing is sparse, i.e. we poll the branch for changes periodically, and
if something has changed we run it through our CI, even if that means
multiple commits/merges.

| ISSUES                                     | Documentation          | Repository                       |
| --                                         | --                     | --                               |
| [drm-tip][]                                | [docs][drm-tip-doc]    | [repo][drm-tip-repo]             |
| [IGT GPU Tools][drm-tip]<sup>\*</sup>      | [docs][igt-doc]        | [repo][igt-repo]                 |
| [Linus' tree][linus]                       | ???                    | [repo][linus-repo]               |
| [linux-next][]                             | [docs][linux-next-doc] | [repo][linux-next-repo]          |
| [drm-intel-fixes][drm-intel-fixes]         | [docs][drm-intel-doc]  | [repo][drm-intel-fixes-repo]     |
| [drm-intel-next-fixes][dinf]               | [docs][drm-intel-doc]  | [repo][dinf-repo]                |
| [drm-intel-next-queued][dinq]              | [docs][drm-intel-doc]  | [repo][dinq-repo]                |
| [drm-misc-fixes][drm-misc-fixes]           | [docs][drm-misc-doc]   | [repo][drm-misc-fixes-repo]      |
| [drm-misc-next-fixes][drm-misc-next-fixes] | [docs][drm-misc-doc]   | [repo][drm-misc-next-fixes-repo] |
| [drm-intel-media][drm-intel-media]         | ???                    | [repo][drm-intel-media-repo]     |
| [Dave Airlie's branch][airlied]            | ???                    | [repo][airlied-repo]             |

*\*: IGT results are part of the drm-tip visualisation*

[drm-tip]:                   /tree/drm-tip/
[drm-tip-repo]:              https://cgit.freedesktop.org/drm-tip
[drm-tip-doc]:               https://drm.pages.freedesktop.org/maintainer-tools/drm-tip.html

[igt-repo]:                  https://gitlab.freedesktop.org/drm/igt-gpu-tools
[igt-doc]:                   https://gitlab.freedesktop.org/drm/igt-gpu-tools#igt-gpu-tools

[linus]:                     /tree/linus/
[linus-repo]:                https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git

[linux-next]:                /tree/linux-next/
[linux-next-repo]:           https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/
[linux-next-doc]:            https://www.kernel.org/doc/man-pages/linux-next.html

[drm-intel-fixes]:           /tree/drm-intel-fixes/
[drm-intel-fixes-repo]:      /tree/drm-intel-fixes/
[dinf]:                      /tree/drm-intel-next-fixes/
[dinf-repo]:                 https://cgit.freedesktop.org/drm-intel/log/?h=drm-intel-next-fixes
[dinq]:                      /tree/drm-intel-next-queued/
[dinq-repo]:                 https://cgit.freedesktop.org/drm-intel/log/?h=drm-intel-next-queued
[drm-intel-doc]:             https://drm.pages.freedesktop.org/maintainer-tools/repositories.html#the-upstream-i915-driver-repository

[drm-misc-fixes]:            /tree/drm-misc-fixes/
[drm-misc-fixes-repo]:       https://cgit.freedesktop.org/drm-misc/log/?h=drm-misc-fixes
[drm-misc-next-fixes]:       /tree/drm-misc-next-fixes/
[drm-misc-next-fixes-repo]:  https://cgit.freedesktop.org/drm-misc/log/?h=drm-misc-next-fixes
[drm-misc-doc]:              https://drm.pages.freedesktop.org/maintainer-tools/drm-misc.html

[drm-intel-media]:           /tree/drm-intel-media/
[drm-intel-media-repo]:      https://cgit.freedesktop.org/~tursulin/drm-intel/?h=media

[airlied]:                   /tree/airlied/
[airlied-repo]:              https://cgit.freedesktop.org/~airlied/linux/log/?h=for-intel-ci


## Results Filtering And Bug Tracking

Bugs caught by our system are filled to the following bug trackers:

 * [freedesktop's bugzilla][fdo-bugzilla] - all things IGT and DRM
 * [kernel.org's bugzilla][korg-bugzilla] - all other upstream bugs

[fdo-bugzilla]:  https://bugs.freedesktop.org/
[korg-bugzilla]: https://bugzilla.kernel.org/

We also maintain a set of filters that tie those bugs to failures we see in
our CI using machine names/types and patterns in dmesg/test output. This way
we are able to filter known issues out of pre-merge results to decrease noise
for developers, keep track of bug's life cycle, and reproduction rate. The
tool makes us able to confirm that a supposed fix is indeed fixing things.

You can **browse the CI issues** with the related data using our tool called
[cibuglog][] (updated hourly).

[cibuglog]: https://intel-gfx-ci.01.org/cibuglog/


## The Kinds Of Runs

### Basic Acceptance Tests (aka BAT)

BAT is the most basic run in our portfolio - it consists of tests that help
us ensure that the testing configuration is in a working condition. It gates
all the sharded runs - if it breaks, then the build is deemed to broken to
use more of the CI time on it. It utilizes [fast-feedback.testlist][], which
you can find in the IGT repository.

[fast-feedback.testlist]: https://gitlab.freedesktop.org/drm/igt-gpu-tools/blob/master/tests/intel-ci/fast-feedback.testlist

### Full IGT (aka sharded runs)

If the BAT run is succesful, then we continue with the sharded run. Much
broader set of tests (everything you can find in IGT filtered through our
[blacklist.txt][]) is executed.

[blacklist.txt]: https://gitlab.freedesktop.org/drm/igt-gpu-tools/blob/master/tests/intel-ci/blacklist.txt


### Idle Runs

Those runs are complementary to the ones above, used mainly to gather extra
data and increase coverage. As the name suggests, they are run when CI would
be idle otherwise (i.e. there nothing to test for the regular
pre-merge/post-merge).

- [drmtip][drmtip-results] - Full IGT but on the same hosts that BAT uses,
  thus increasing coverege.
- [KASAN][kasan-results] - same as above but runs with
  [The Kernel Address Sanitizer][kasan] enabled.
- 100 re-runs - re-run BAT 100 times with fixed IGT and kernel for extra data
  on flip-floppers.

[drmtip-results]: /tree/drm-tip/drmtip.html
[kasan-results]:  /tree/drm-tip/kasan.html

[kasan]: https://www.kernel.org/doc/html/latest/dev-tools/kasan.html

## Contacts

 * **IRC:** #intel-gfx-ci @ freenode
 * **hardware/CI maintainer:** Tomi Sarvela - tomi.p.sarvela @ intel.com
 * **cibuglog/metrics maintainer:** Martin Peres - martin.peres @ intel.com
