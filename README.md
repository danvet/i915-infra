# i915 Infra

## Contributing

By contributing to this project you accept and agree to the terms and
conditions, for your contributions, present in [DCO](/site) and
[LICENSE](/LICENSE) (MIT).

The webpage contents that reside under [site/](/site) directory of this
repository are licensed under [site/LICENSE](/site/LICENSE) (CC BY 4.0).

Please add your `Signed-off-by: Name <e-mail>` line to the commits you submit
as an acknowledgement.

To contribute send us a merge request via GitLab.


## The Site

The files behind <http://intel-gfx-ci.01.org/>.

There are two directories here: `site/` with all the markdown files, and
`site-publishing/` with all the templates and code that generate the actual,
viewable, static webpage.

### Building

To generate the html files you need to have [Ruby][] with [Bundler][]
installed. Additionally, because of a dependency, [nokogiri][], you need to
install C toolchain, libxml and Ruby development packages.

Then just:
```
cd site-publishing/
bundle install --path bundle
bundle exec nanoc
```

If everything went fine, the html files should be in `site-publishing/output/`

You can use `nanoc view` to start a local webserver listening on
<http://localhost:3000>.

[Ruby]:  https://www.ruby-lang.org/en/
[Bundler]: https://bundler.io/
[nokogiri]: https://www.nokogiri.org/
