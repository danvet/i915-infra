require 'nokogiri'
require 'kramdown'

Nanoc::Filter.define(:md_links_to_html) do |content, params|
  dom = Nokogiri::HTML(content)
  dom.css('a').each do |a|
    uri = URI(a['href'])
    unless uri.host
      uri.path.sub!(/\.md$/, '.html')
      a['href'] = uri.to_s
    end
  end
  dom.to_html
end

module Custom
  def create_tree(tree_name)
    @config[:view_types].each_pair do |file, view_type|
      attributes = { tree: tree_name,
                     current_tree_view: file,
                     current_tree_view_type: view_type }
      item = @items.create("", attributes, "/tree/#{tree_name}/#{file}")
    end
  end

  def create_pre_merge_templates(tree_name)
    @config[:view_types].each_pair do |file, view_type|
      attributes = { tree: tree_name,
                     current_tree_view: file,
                     current_tree_view_type: view_type,
                     pre_merge: true }
      item = @items.create("", attributes, "/tree/#{tree_name}/pre-merge-templates/#{file}")
    end
  end

  def create_toc_and_title_from_headings(item)
    html = Kramdown::Document.new(item.raw_content).to_html

    dom = Nokogiri::HTML(html)
    h1 = dom.css('h1').first
    item[:title] = h1 ? h1.text : 'Untitlted'

    item[:toc] = dom.css('h1, h2, h3, h4, h5, h6').map do |elem|
      { level: elem.name[1].to_i,
        anchor: elem['id'],
        text: elem.text }
    end
  end
end

module HardwareList
  def create_hardware_list(items)
    hw_raw = HWParse.parse('../hardware')
    hw = hw_raw
      .group_by {|x| x[0].start_with?("fi-") ? x[-1]['gen'] : x[0].split('-')[0]}
      .sort_by {|x| x.first.is_a?(String) ? 0 : x.first } # put shards and pigs last
      .reverse # newest first

    content = "# Hardware List\n\n"
    content += "[raw yaml](/hardware.yml)\n\n"
    hw.each do |gen|
      if gen.first.is_a? Numeric
        content += "## Gen#{gen.first}\n\n"
      else
        content += "## #{gen.first}s\n\n"
      end
      gen.last.to_h.each_pair do |host, data|
        content += "### #{host}\n"
        content += "[raw data](/hardware/#{host}/)\n"
        content += "<pre>#{data.to_yaml.lines[1..-1].join}</pre>\n\n"
      end
    end

    items.create(hw_raw.to_yaml, {}, '/hardware.yml')
    items.create(content, {}, '/hardware.md')
  end
end

use_helper Custom
use_helper HardwareList
use_helper Nanoc::Helpers::Rendering
